const expect = require("chai").expect;
const assert = require("chai").assert;
const should = require("chai").should();
const mylib = require("../src/mylib");

describe("Unit testin mylib.js", () => {

    let myvar = undefined;
    before(() => {
        myvar = 1;
    })
    it("Should return 2 when using sum function with a=1, b=1", () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })
    it("Should return 3 when using mult function with a=5, b=2 ", () => {
        const tot = mylib.sub(5,2)
        expect(tot).to.equal(3)
    })
    it.skip("Assert foo is not bar", () => {
        assert("foo" !== "bar")
    })
    it("Myvar should exist", () => {
        should.exist(myvar)
    })
    after(() => {
        console.log("hello")
    })
})